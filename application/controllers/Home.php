<?php defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
    }

    public function index()
    {
        // $this->crud_md->set_table('banners');
        // $data['banners'] = $this->crud_md->get_data_by_order(4, 0, 'row_order', 'asc');

        $data['page_title']   = "Əsas Səhifə";

        if(empty($this->session->userdata('logged_in'))){
            $data['posts'] = [];
            $data['categories'] = [];

            $this->load->front('pages/home', $data);
        }

        $this->load->front('pages/home', $data);
    }
}
