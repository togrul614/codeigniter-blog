<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model('Users_model','users_md');
    }


    public function sign_up()
    {
        $data['page_title']   = "Register";


        $this->load->front('auth/register', $data);
    }

    public function register()
    {
        if($this->input->method() == 'post'){
            
            $request_data = [
                'name' => $this->security->xss_clean($this->input->post('first_name')),
                'surname' => $this->security->xss_clean($this->input->post('last_name')),
                'email' => $this->security->xss_clean($this->input->post('email')),
                'password' => sha1($this->input->post('password')),
            ];
            
            
            $this->form_validation->set_rules($this->register_rules());
            $this->form_validation->set_data($request_data);

            if($this->form_validation->run() == FALSE){
                echo validation_errors();
                redirect('/sign_up'); 
            }
            
            if(!empty($_FILES['photo']['name'])){
                $config['upload_path']          = 'uploads/';
                $config['allowed_types']        = 'gif|jpg|png';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('photo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);exit();
                        // redirect('/sign_up'); 
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $request_data['photo'] = $data['upload_data']['file_name'];
                }
            }
            
            $query = $this->users_md->insert($request_data);

            if($query > 0){
                $this->session->set_flashdata('message','You registered successfully');

                redirect('sign_in');
            }else{
                $this->session->set_flashdata('message','There is error in registration');

                redirect('sign_up');
            }
            
        }
    }

    public function sign_in(){

        $data['page_title']   = "Login";


        $this->load->view('front/auth/login', $data);
    }

    public function login(){
        if($this->input->method() == 'post'){
            
            $request_data = [
                'email' => $this->security->xss_clean($this->input->post('email')),
                'password' => sha1($this->input->post('password')),
            ];
            
            $this->form_validation->set_rules($this->login_rules());
            $this->form_validation->set_data($request_data);

            if($this->form_validation->run() == FALSE){
                echo validation_errors();
                redirect('/sign_in'); 
            }
            
            $user_info = $this->users_md->getLoginData($request_data);
            
            if(!empty($user_info)){
                $user_data = [
                    'name' => $user_info->name,
                    'email' => $user_info->email,
                    'logged_in' => 1
                ];

                $this->session->set_userdata($user_data);

                $this->session->set_flashdata('message','You are logged');

                redirect('home');
            }else{
                $this->session->set_flashdata('message','There is error in login');

                redirect('sign_in');
            }
            
        }
    }

    protected function register_rules()
    {
        $config = array(
                array(
                    'field' => 'name',
                    'label' => 'First Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'surname',
                    'label' => 'Last Name',
                    'rules' => 'required'
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required',
                        'errors' => array(
                                'required' => 'You must provide a %s.',
                        ),
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email|is_unique[users.email]'
                )
        );
        
        return $config;

    }

    protected function login_rules()
    {
        $config = array(
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required',
                        'errors' => array(
                                'required' => 'You must provide a %s.',
                        ),
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email'
                )
        );
        
        return $config;

    }
}
