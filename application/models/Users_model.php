<?php 

class Users_model extends CI_Model {

    protected $table = 'users';

    public function getLoginData($data)
    {
        $this->db->where('email',$data['email']);
        $this->db->where('password',$data['password']);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function insert($data)
    {
            $this->db->insert($this->table, $data);

            return $this->db->insert_id();
    }

    public function update_entry()
    {
            $this->title    = $_POST['title'];
            $this->content  = $_POST['content'];
            $this->date     = time();

            $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}