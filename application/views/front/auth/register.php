<div class="row g-12">
      <div class="col-md-12 col-lg-12">
        <h4 class="mb-3 text-center">Register</h4>
        <?php echo validation_errors(); ?>
       <?= form_open_multipart('register'); ?>
          <div class="row g-3">
            <div class="col-sm-6">
              <label for="firstName" class="form-label">First name</label>
              <input type="text" class="form-control" id="firstName" name="first_name" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>

            <div class="col-sm-6">
              <label for="lastName" class="form-label">Last name</label>
              <input type="text" class="form-control" id="lastName" name="last_name" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>

            <div class="col-12">
              <label for="email" class="form-label">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" required>
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="col-12">
              <label for="password" class="form-label">password</label>
              <input type="password" class="form-control" id="password" name="password" required>
              <div class="invalid-feedback">
                Please enter your password.
              </div>
            </div>

            <div class="col-12">
              <label for="photo" class="form-label">Photo</label>
              <input type="file" class="form-control" name="photo" id="photo">
              <div class="invalid-feedback">
                Please enter a valid photo
              </div>
            </div>
          <hr class="my-4">

          <button class="w-100 btn btn-primary btn-lg" type="submit">Continue to checkout</button>
        <?= form_close(); ?>
      </div>
    </div>