<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_settings extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'key' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '55',
                        ),
                        'value' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '500',
                        ),
                        'status' => array(
                            'type' => 'TINYINT',
                            'constraint' => 5,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('settings');
        }

        public function down()
        {
                $this->dbforge->drop_table('settings');
        }
}