<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_categories extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'description' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '500',
                        ),
                        'content' => array(
                            'type' => 'TEXT',
                        ),
                        'author_id' => array(
                            'type' => 'TINYINT',
                            'constraint' => '5',
                        ),
                        'image' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'image_alt' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'tags' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'status' => array(
                            'type' => 'TINYINT',
                            'constraint' => 5,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('categories');
        }

        public function down()
        {
                $this->dbforge->drop_table('categories');
        }
}