<?php  defined('BASEPATH') OR exit('No direct script access allowed'); 


if (!function_exists('dd'))
{
	function dd($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit();
	}
}

if(!function_exists('isLogin'))
{
	function isLogin()
	{
		if(empty($_SESSION['logged_in']))
		{
			redirect(base_url('/sign_in'));
		}
	}
}


?>